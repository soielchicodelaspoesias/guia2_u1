#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
   // metodos privados
    private:
        bool band;
        int tope = 0;
    // metodos publicos
    public:
        Pila ();
        Pila (int tope, bool band);
        void pila_llena(int tope, bool &band, int max);
        void push(string **puerto, int &tope, int n_pilas, bool band, int max, int pila, string codigo);
        void pop(string **puerto, int &tope, int n_pilas, bool band, int pila_eleminar, int posicion);
        void imprimir_pila(string **puerto, int n_pilas, int max);
};
#endif
