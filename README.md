# guia0_unidad1

*Puerto de containers*                                                                                             
(P.C)

Guarda la cantidad de container que quieras en un puesto del tamaño que desees                                                                      

*Pre-requisitos* 

C++
make                                                                                                                              
*Problematica del ejecicio*

Se necesita crear un programa que apile containers en un puerto del tamaño mxn (ingresados por el usuario), si se necesita sacar un container que esta debajo de otros container se necesitan cambiar de posicion para despues retirar ese container, cada movimiento debe ser mostrado en la terminal

*Ejecutando*                                                                                                                                         
AL ejecutar el programa se le pide al usurio la cantidad de pilas y la cantidad maxima de container que va a tener cada pila, se recomineda que la cantidad de pilas sea igual al maximo para que el programa funcione al 100%,
, posteriomente se encontrara con un menu que le pedira que desea hacer, si desea agregar para seleccionar la pila desea las pilas estan enumeradas del 0 hasta la cantidad de pilas-1, le pide un codigo de 4 caracteres mas que nada para que no se desalinie el puerto. Si desea sacar alguno debe sacar los que estan arriba de el si es que hay, para eso debe indicar la pila a la cual desea moverla.

*Construido con*                                                                                                                                    
C++                                                                                                           

*Versionado*                                                                                                                                        
Version 0,1                                                                                                                                       

*Autor*                                                                                                                                           
Luis Rebolledo                                                                                                                                                                                                                                                                                                                        

