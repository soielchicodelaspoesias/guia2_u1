#include <iostream>
#include "Pila.h"
using namespace std;

Pila::Pila() {
    bool band;
    int tope;
}

Pila::Pila (int tope, bool band) {
    this->band = band;
    this->tope = tope;
}
// si la pila esta llena band es true
void Pila::pila_llena(int tope, bool &band, int max){
  if (tope == max){
    this->band = true;
  }else{
    this->band = false;
  }
}
// se agrega un string en la matriz
void Pila::push(string **puerto, int &tope, int n_pilas, bool band, int max, int pila, string codigo){
  pila_llena(tope, band, max);
  if (this->band == true){
    cout << "Pila llena" << endl;
  }
  else{
    puerto[tope][pila] = codigo;
    this->tope = tope;
  }
}
// se elimina el string de la cadena
void Pila::pop(string **puerto, int &tope, int n_pilas, bool band, int pila_eleminar, int posicion){
    // se reemplaza el string por cuatro espacios
    puerto[pila_eleminar][posicion] = "    ";
    this->tope = tope;
  }
// se imprime la lista
void Pila::imprimir_pila(string **puerto, int n_pilas, int max) {
    for(int i=n_pilas-1;i>=0;i--){
      for(int j=0;j<max;j++){
        cout << "|"<<puerto[i][j] << "|";
      }
      cout << endl;
    }
}
