#include <iostream>
#include <string>
#include "Pila.h"
using namespace std;

void menu(string **puerto, int topes[], int n_pilas, bool &band, int max);
// Se llena la matriz de espacios de cuatro espacios
void llena(string **&puerto, int n_pilas, int max){
  for(int i=0;i<n_pilas;i++){
    for(int j=0;j<max;j++){
      puerto[i][j] = "    ";
    }
  }
}
// Se busca el codigo del container a eleminar y se guarda la posicion i y la posicion j del elemto a eleminar
void buscar(string eleminar, string **&puerto, int n_pilas, int max, int &pila_eleminar, int &posicion){
  for(int i=0;i<n_pilas;i++){
    for(int j=0;j<max;j++){
      if(puerto[i][j]==eleminar){
        pila_eleminar = i;
        posicion = j;
      }
    }
  }
}
// funcion que cambia los container si existe un container arriba
void cambia(Pila pi, string **&puerto, int n_pilas, int max, string eleminar, int posicion, int pila_eleminar, bool &band, int topes[]){
  // se crean las variables
  string codigo;
  int auxiliar;
  // recorre la pila donde se encuentra el container a remover, desde la posicion hacia la posicion final
  for(int i=n_pilas-1;i>=pila_eleminar;i--){
    // La posicion de la matriz tiene que ser distinto del que se desea remover o de los cuatro espacios
    if(puerto[i][posicion]!= eleminar & puerto[i][posicion] != "    "){
      int pila;
      // el codigo es el container que se mueve
      codigo = puerto[i][posicion];
      // se pide la pila a la que se desea mover
      cout << "A que pila desea mover " << codigo << ": ";
      cin >> pila;
      // se obtine el tope de la pila
      int tope = topes[pila];
      cout << tope;
      if(tope==n_pilas){
        cout << "No se pudo mover por que la pila esta llena, intente nuevamente" << endl;
        pi.imprimir_pila(puerto, n_pilas, max);
        menu(puerto, topes, n_pilas, band, max);;
      }
      // se agrega otra pila
      pi.push(puerto, tope, n_pilas, band, max, pila, codigo);
      // se elemina el que se agrego en la otra pila
      puerto[i][posicion] = "    ";
      // se le resta a la pila del container cambiado y se le suma al que se le agrego
      topes[pila_eleminar]--;
      topes[pila]++;
      // se imprime la matriz para observar el moviminto
      pi.imprimir_pila(puerto, n_pilas, max);
    }
  }
}
// Busca si el container ya esta dentro de la matriz y retorna el contador
int existencia(string **&puerto, int n_pilas, int max, string codigo){
  int contador = 0;
  for(int i=0;i<n_pilas;i++){
    for(int j=0;j<max;j++){
      if(puerto[i][j]==codigo){
        // si ya esta el mismo string le suma al contador
        contador++;
      }
    }
  }
  return contador;
}
void menu(string **puerto, int topes[], int n_pilas, bool &band, int max){
  // constructor de la clase
  Pila pi = Pila();
  int opcion;
  int tope;
  string codigo;
  do {

    cout << "----------MENU----------" << endl;
    cout << "1.- Agregar" << endl;
    cout << "2.- Sacar container" << endl;
    cout << "3.- Salir" << endl;
    cout << "------------------------" << endl;

    cout << "Ingrese una opcion: ";
    cin >> opcion;

    switch(opcion){
      case 1:
      int contador;
      int pila;
      // le pide la pila al que se va a ingresar
      cout << "Ingrese la pila : ";
      cin >> pila;
      // se obtiene el tope de esa pila
      tope = topes[pila];
      // se pide el codigo del container
      cout << "Ingrese el codigo del container de cuatro caracteres alfanumericos: ";
      cin >> codigo;
      // si el codigo es de tamaño menos o mayor a 4 llama al MENU
      // esto es para que no se desalinie la matriz, es estetico, se ve mas ordenado
      if(codigo.size()>4 || codigo.size() < 4){
        cout << "El codigo que ingreso no tiene cuatro caracteres" << endl;
        menu(puerto, topes, n_pilas, band, max);
        break;
      }
      contador = existencia(puerto, n_pilas, max, codigo);
      // si contador es igual a 1 significa  que ya existe el container
      if(contador==1){
        cout << "Ese container ya esta registrado" << endl;
        // se llama a menu
        menu(puerto, topes, n_pilas, band, max);
        break;
      }
      // se agrega el nuevo container
      pi.push(puerto, tope, n_pilas, band, max, pila, codigo);
      // se suma 1 al tope de la pila
      topes[pila]++;
      pi.imprimir_pila(puerto, n_pilas, max);
      break;

      case 2:
      int pila_eleminar;
      int posicion;
      string eleminar;
      // Se pide el nombre del container a eleminar
      cout << "Ingrese el contedor a sacar: ";
      cin >> eleminar;
      // se llama a buscar para obtener la posicion
      buscar(eleminar, puerto, n_pilas, max, pila_eleminar, posicion);
      // Se obtiene el tope de la pila en la que se encuentra el container a eleminar
      tope = topes[pila_eleminar];
      cambia(pi, puerto, n_pilas, max, eleminar, posicion, pila_eleminar, band, topes);
      pi.pop(puerto, tope, n_pilas, band, pila_eleminar, posicion);
      // una vez eliminado se le resta uno a la pila
      topes[pila_eleminar]--;
      cout << "\n";
      // imprime la matriz
      cout << "Se retiro el container deseado" << endl;
      pi.imprimir_pila(puerto, n_pilas, max);
      break;
      }
   }while (opcion != 3);
}

int main() {
  int n_pilas;
  int max;
  bool band;
  // Se pide el tamaño del puerto
  cout << "Ingrese el numero de pilas: ";
  cin >> n_pilas;
  cout << "Ingrese la maxima cantidad de contenedores: ";
  cin >> max;

  // Crea una lista de topes para que cada pila tenga un tope diferente
  int topes[n_pilas];
  for(int i=0;i<n_pilas;i++){
    topes[i] = 0;
  }
  // Inicia la matriz
  string **puerto= new string*[n_pilas];
  for(int i=0;i<n_pilas;i++){
    puerto[i] = new string[max];
  }
  // llama a las funciones
  llena(puerto, n_pilas, max);
  menu(puerto, topes, n_pilas, band, max);

   return 0;
}
